package api

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"net/http"
	"time"

	"github.com/advancing-life/rabbit-can-middleware/controllers/docker"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo"
)

type (
	// SocketConnectionData ...
	SocketConnectionData struct {
		URL         string `json:"url"`
		ContainerID string `json:"container_id"`
		RESULT      string `json:"result"`
	}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:    4096,
	WriteBufferSize:   4096,
	EnableCompression: true,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// GetMD5Hash ...
func GetMD5Hash() string {
	hasher := md5.New()
	hasher.Write([]byte(time.Time.String(time.Now())))
	return hex.EncodeToString(hasher.Sum(nil))
}

// Connection ...
func Connection(c echo.Context) error {
	key := GetMD5Hash()

	value, err := docker.Mk(key, c.Param("lang"))
	if err != nil {
		c.Logger().Error(err)
		return c.String(500, "Docker is Panic")
	}

	res := &SocketConnectionData{
		URL:         "ws://localhost:1234/api/v1/socket/" + key,
		ContainerID: key,
		RESULT:      value,
	}
	return c.JSON(http.StatusOK, res)

}

// Socket is ...
func Socket(c echo.Context) error {

	ws, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return err
	}

	defer ws.Close()

	for {
		var rcv docker.ExecutionCommand
		execChan := make(chan docker.ExecutionCommand)

		if err := ws.ReadJSON(&rcv); err != nil {
			return err
		}
		fmt.Printf("Receive data=\x1b[36m%#v\x1b[0m\n", rcv)

		go docker.Exec(execChan, rcv, c.Param("name"))

		for v := range execChan {
			if err := ws.WriteJSON(v); err != nil {
				return err
			}
			fmt.Printf("Send data=\x1b[36m%#v\x1b[0m\n", v)
		}
	}
}

// Socket ...
// func Socket(c echo.Context) error {
// 	websocket.Handler(func(ws *websocket.Conn) {
// 		defer ws.Close()
// 		for {
// 			execmd, err := receive(ws)
// 			if err != nil {
// 				ws.Close()
// 				c.Logger().Error(err)
// 			}
//
// 			exec := make(chan docker.ExecutionCommand)
// 			go docker.Exec(exec, execmd, c.Param("name"))
//
// 			for v := range exec {
// 				err := send(ws, v)
// 				if err != nil {
// 					ws.Close()
// 				}
// 			}
// 		}
// 	}).ServeHTTP(c.Response(), c.Request())
// 	return nil
// }
//
// func send(ws *websocket.Conn, send docker.ExecutionCommand) (err error) {
// 	err = websocket.JSON.Send(ws, send)
// 	fmt.Printf("Send data=\x1b[36m%#v\x1b[0m\n", send)
// 	return
// }
//
// func receive(ws *websocket.Conn) (rcv docker.ExecutionCommand, err error) {
// 	err = websocket.JSON.Receive(ws, &rcv)
// 	if err != nil {
// 		return
// 	}
// 	fmt.Printf("Receive data=\x1b[36m%#v\x1b[0m\n", rcv)
// 	return
// }
